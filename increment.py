import sublime, sublime_plugin

class IncrementGetInputCommand(sublime_plugin.WindowCommand):
	def run(self):
		self.window.show_input_panel("Increase/Decrease with:", "", self.on_done, None, None)
		pass

	def on_done(self, input):
		try:
			by = int(input)
			if self.window.active_view():
				self.window.active_view().run_command("increment", {"by": by} )
		except ValueError:
			pass

class IncrementCommand(sublime_plugin.TextCommand):
	def run(self, edit, by = 1):
		for region in self.view.sel():
			if not region.empty():
				try:
					number = int(self.view.substr(region)) + by
					self.view.replace(edit, region, str(number))
				except ValueError:
					pass

# README #

### What is this repository for? ###

* Increment highlighted number plugin for sublime text
* Work with multi cursor
* Can increment with 1, take an argument or take user input
* Version 1.0.0

### Install instructions ###
* working on it

### Key bindings ###

    { "keys": ["ctrl+alt+i"], "command": "increment" } // increment by 1 by default
    { "keys": ["ctrl+alt+shift+i"], "command": "increment", "args": {"by": -1 } } // decrease by one with argument, change to any number to match your needs
    { "keys": ["ctrl+alt+o"], "command": "increment_get_input" } // open a window and take user input and sends it as an argument to increment

### Credits ###

* Christoffer "Leulas" Perming
* Robin "Nanosekund" Persson